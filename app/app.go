package app

import (
	"aibook/middleware"
	"aibook/model"
	"aibook/service"

	"go.uber.org/fx"
)

func New() *fx.App {
	return fx.New(
		logModule,
		// 注入db模块
		dbModule,
		// 注入config模块
		configModule,
		// 注入http模块
		httpModule,
		// 注入缓存模块
		cacheModule,
		// 注入业务模块
		service.ServiceModule,
		// 注入model模块
		model.ModelModule,
		// 注入中间件模块
		middleware.MiddlewareModule,
	)
}
