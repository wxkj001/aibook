package app

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/storage/memory/v2"
	"github.com/gofiber/storage/redis/v3"
	"github.com/spf13/viper"
	"go.uber.org/fx"
)

var cacheModule = fx.Module("cacheModule", fx.Provide(NewCache))

func NewCache(config *viper.Viper) fiber.Storage {
	config.GetString("cache.driver")
	switch config.GetString("cache.driver") {
	case "memory":
		return memory.New(memory.Config{
			GCInterval: config.GetDuration("cache.memory.interval"),
		})
	case "redis":
		return redis.New()
	default:
		return nil
	}
}
