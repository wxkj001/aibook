package app

import (
	"context"
	"strings"

	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	"go.uber.org/fx"
	"xorm.io/xorm"
)

var dbModule = fx.Module("dbModule", fx.Provide(NewDB))

func NewDB(config *viper.Viper, lc fx.Lifecycle) (*xorm.Engine, error) {
	pgurl := []string{
		"postgres://",
		config.GetString("db.user"),
		":",
		config.GetString("db.password"),
		"@",
		config.GetString("db.host"),
		":",
		config.GetString("db.port"),
		"/",
		config.GetString("db.dbname"),
		"?sslmode=disable",
	}
	engine, err := xorm.NewEngine("postgres", strings.Join(pgurl, ""))
	if strings.ToUpper(config.GetString("log.level")) == "DEBUG" {
		engine.ShowSQL(true)
	}
	if err != nil {
		return nil, err
	}
	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error { return engine.Sync2() },
		OnStop:  func(ctx context.Context) error { return engine.Close() },
	})
	return engine, nil
}
