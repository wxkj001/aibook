package app

import (
	"aibook/public"
	"aibook/router"
	"context"
	"errors"
	"net/http"

	"github.com/goccy/go-json"
	"github.com/gofiber/fiber/v2/middleware/filesystem"
	"github.com/spf13/viper"

	"github.com/gofiber/fiber/v2"
	"go.uber.org/fx"
	"go.uber.org/zap"
)

var httpModule = fx.Module("httpServer", fx.Provide(fx.Annotate(NewHttp, fx.ParamTags(`group:"routes"`))), fx.Invoke(func(*fiber.App) {}))

func NewHttp(routers []router.Route, config *viper.Viper, lc fx.Lifecycle, log *zap.Logger) *fiber.App {
	web := fiber.New(fiber.Config{
		JSONEncoder: json.Marshal,
		JSONDecoder: json.Unmarshal,
		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			// Status code defaults to 500
			code := fiber.StatusInternalServerError
			// Retrieve the custom status code if it's a *fiber.Error
			var e *fiber.Error
			if errors.As(err, &e) {
				code = e.Code
			}

			// Send custom error page
			err = ctx.Status(code).JSON(router.Response{
				Code:    code,
				Message: e.Message,
				Data:    nil,
			})
			if err != nil {
				// In case the SendFile fails
				return ctx.Status(fiber.StatusInternalServerError).SendString("Internal Server Error")
			}

			// Return from handler
			return nil
		},
	})
	web.Use("/", filesystem.New(filesystem.Config{
		Root:  http.FS(public.PublicIndexFs),
		Index: "web/dist/index.html",
	}))
	web.Use("/assets", filesystem.New(filesystem.Config{
		Root:       http.FS(public.PublicAssetsFs),
		PathPrefix: "web/dist/assets",
		Browse:     true,
	}))
	api := web.Group("/api")
	for _, v := range routers {
		v.RouteRegister(api)
	}
	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			if config.GetString("tls.cert") != "" && config.GetString("tls.key") != "" {
				go web.ListenTLS(":"+config.GetString("web.port"), config.GetString("tls.cert"), config.GetString("tls.key"))
			} else {
				go web.Listen(":" + config.GetString("web.port"))
			}
			return nil
		},
		OnStop: func(ctx context.Context) error {
			return web.Shutdown()
		},
	})
	return web
}
