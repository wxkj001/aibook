package app

import (
	"strings"

	"github.com/spf13/viper"
	"go.uber.org/fx"
	"go.uber.org/zap"
)

var logModule = fx.Module("logModule", fx.Provide(NewLog))

func NewLog(config *viper.Viper) (*zap.Logger, error) {
	cf := zap.NewProductionConfig()

	switch strings.ToUpper(config.GetString("log.level")) {
	case "DEBUG":
		cf.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
	case "INFO":
		cf.Level = zap.NewAtomicLevelAt(zap.InfoLevel)
	case "WARN":
		cf.Level = zap.NewAtomicLevelAt(zap.WarnLevel)
	case "ERROR":
		cf.Level = zap.NewAtomicLevelAt(zap.ErrorLevel)
	}
	log, err := cf.Build()
	if err != nil {
		return nil, err
	}
	return log, nil
}
