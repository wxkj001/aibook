package middleware

import (
	"github.com/gofiber/fiber/v2"
	"go.uber.org/fx"
)

var MiddlewareModule = fx.Module("middlewareModule", fx.Provide(NewMiddleware))

type MiddlewareResult struct {
	fx.Out
	*Middlewares
}
type MiddlewareParams struct {
	fx.In
	*Middlewares
}
type Middlewares struct {
	SignMiddleware *SignMiddleware
}

func NewMiddleware(store fiber.Storage) MiddlewareResult {
	return MiddlewareResult{Middlewares: &Middlewares{SignMiddleware: NewSign(store)}}
}
