package model

import (
	"go.uber.org/fx"
	"xorm.io/xorm"
)

var ModelModule = fx.Module("modelModule", fx.Provide(NewModel))

type ModelResult struct {
	fx.Out
	*Models
}
type ModelParams struct {
	fx.In
	*Models
}
type Models struct {
	UserModel *UserMode
}

func NewModel(db *xorm.Engine) (ModelResult, error) {
	return ModelResult{Models: &Models{
		UserModel: NewUserModel(db),
	}}, nil
}
