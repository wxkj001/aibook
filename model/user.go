package model

import (
	"crypto/md5"

	"xorm.io/xorm"
)

func NewUserModel(db *xorm.Engine) *UserMode {
	user := &UserMode{db: db}
	db.Sync2(user)
	return user
}

type UserMode struct {
	db         *xorm.Engine
	Id         int     `json:"id" xorm:"pk autoincr notnull unique index int4 'id'"`
	Nickname   string  `json:"nickname" xorm:"notnull varchar(100) 'nickname'"`
	PassWord   string  `json:"password" xorm:"null varchar(100) 'password' ->"`
	Email      string  `json:"email" xorm:"notnull varchar(100) 'email'"`
	Phone      string  `json:"phone" xorm:"null varchar(100) 'phone'"`
	Avatar     string  `json:"avatar" xorm:"notnull varchar(255) 'avatar'"`
	Role       int     `json:"role" xorm:"notnull int 'role'"`
	Ban        bool    `json:"ban" xorm:"notnull bool 'ban'"`
	InviteUid  int     `json:"inviteuid" xorm:"null int4 'invite_uid'"`
	Integer    float64 `json:"integer" xorm:"notnull decimal(10,2) 'integer' default(0.00)"`
	LoginTime  int64   `json:"logintime" xorm:"null timestamp 'logintime'"`
	CreateTime int64   `json:"createtime" xorm:"notnull timestamp 'createtime'"`
	UpdateTime int64   `json:"updatetime" xorm:"notnull timestamp 'updatetime'"`
	DeleteTime int64   `json:"deletetime" xorm:"null timestamp 'deletetime'"`
}

func (u *UserMode) pass(pass string) string {
	return string(md5.New().Sum([]byte(pass)))
}
func (u *UserMode) TableName() string {
	return "users"
}

// 新增用户
func (u *UserMode) CreateUser(user UserMode) (int64, error) {
	total, err := u.db.Count(u)
	if err != nil {
		return 0, err
	}
	if total == 0 {
		// 管理员
		user.Role = 1
	} else {
		// 普通用户
		user.Role = 2
	}
	if user.PassWord != "" {
		user.PassWord = u.pass(user.PassWord)
	}
	return u.db.Insert(user)
}

// 查询用户
func (u *UserMode) GetUser(user *UserMode) (bool, error) {
	return u.db.Get(&user)
}

// 更新用户
func (u *UserMode) UpUser(user *UserMode) (int64, error) {
	return u.db.ID(user.Id).Update(&user)
}

// 积分变动 +1 +-1
func (u *UserMode) UpInteger(id int, oldInteger float64, integer float64) (int64, error) {
	return u.db.ID(id).Cols("integer").Update(UserMode{Integer: oldInteger + integer})
}
