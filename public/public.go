package public

import "embed"

//go:embed web/dist/index.html
var PublicIndexFs embed.FS

//go:embed web/dist/*
var PublicAssetsFs embed.FS
