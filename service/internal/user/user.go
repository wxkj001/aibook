package user

import (
	"aibook/middleware"
	"aibook/model"
	"encoding/hex"

	cryptobin "github.com/deatil/go-cryptobin/cryptobin/rsa"
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

func NewUser(log *zap.Logger, signMiddle middleware.MiddlewareParams, model model.ModelParams) *UserRouter {
	return &UserRouter{log: log, middle: signMiddle, model: model}
}

type UserRouter struct {
	middle middleware.MiddlewareParams
	log    *zap.Logger
	model  model.ModelParams
}

func (u *UserRouter) RouteRegister(r fiber.Router) {
	user := r.Group("/user")
	// u.log.Sugar().Debugf("%+v", u.middle.SignMiddleware.Verify())
	// user.Use(u.middle.SignMiddleware.Verify)
	// user.Get("/index", u.Index)
	// user.Get("/m", u.Jia)
	user.Post("/login", u.Index)
	user.Post("/register", u.Index)
	user.Get("/u/:id", u.Index)
	user.Put("/u/:id", u.Index)

}
func (u *UserRouter) Index(ctx *fiber.Ctx) error {

	return ctx.SendString("User")
}
func (u *UserRouter) Jia(ctx *fiber.Ctx) error {
	u.log.Debug("home", zap.String("h", ctx.Hostname()))
	rsa := cryptobin.NewRsa()
	// hex.EncodeToString([]byte(pub))
	u.log.Sugar().Infof("EncodeToString %+v", hex.EncodeToString([]byte("")))
	pubkey, _ := hex.DecodeString("2d2d2d2d2d424547494e20525341205055424c4943204b45592d2d2d2d2d0a4d49494243674b434151454174737a37635549557750686b71453447757550715655337158736442726e69366e62434a37747964664154756e4e723469514b6b676450546469346d5a7236336a6f4f7665526e35637147346a6f4242315a32736837435154332b6d5258334b74444849565a7a50435348347974746d684f6a5966417a2f4e704e2f6b426331313864554b2f4b327378715966356e55583064764e326a544637574767344758624755524b6741704f61712b7a4a67356c71316f6334432f786c2b75324e6e7a5173654c56484f4150376b392f4a33536b52415772376a586f6c7a565841636f425143355930657848437046516f396d796138506c544e706d5a43696f44334a4b6e4233726d552f714370314472745a4f387550714f785239745130756c52782b7955774f356d57624e3853754e46733556466b4b7a444875482b327a3255576164324452324e7473493434666d6e416d64614974774944415141420a2d2d2d2d2d454e4420525341205055424c4943204b45592d2d2d2d2d")
	rsaPubKey := rsa.
		FromBase64String("").
		FromPublicKey([]byte(pubkey)).
		SetSignHash("SHA256").
		Verify([]byte(`测试`)).
		ToVerify()
	u.log.Sugar().Infof("rsaPubKey %+v", rsaPubKey)
	if !rsaPubKey {
		return ctx.SendString("验签失败")
	}
	return ctx.SendString("验签成功")
}
