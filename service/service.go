package service

import (
	"aibook/router"
	"aibook/service/internal/user"

	"go.uber.org/fx"
)

var ServiceModule = fx.Module("serviceModule", fx.Provide(router.AsRoute(user.NewUser)))
